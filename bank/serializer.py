from rest_framework import serializers
from .models import ExternalTransfer, Account


class ExternalTransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExternalTransfer
        fields = ("id", "token", "amount", "sender_account", "receiver_account", "text")

    def create(self, validated_data):
        return ExternalTransfer.objects.create(**validated_data)

    # lookup_field = 'token'


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ("id", "user", "name", "balance")

    # lookup_field = 'token'


class UpdateExternalTransferStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExternalTransfer
        fields = ("id", "status")
        lookup_field = "token"
        read_only_fields = (
            "token",
            "amount",
            "sender_account",
            "receiver_account",
            "text",
        )
