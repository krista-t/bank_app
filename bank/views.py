from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout  # new
from django.contrib.auth.models import User
from .errors import InsufficientFunds
from bank.models import Account, Transfer, Customer, LoanAccount, ExternalTransfer
from django.db.models import Q
import django
from decimal import Decimal
from django import forms
from .forms import AccountForm, TransferForm, CustomerForm, NewUserForm, LoanPaymentForm
import io
from django.http import FileResponse, HttpResponse
from .utils import render_to_pdf


def home(request):
    return redirect("account/login")


@login_required
def dashboard(request):
    if request.user.is_staff:
        bank_accounts = Account.objects.filter(user=request.user)
        user_accounts = Account.objects.filter(user__is_staff=False)
        customer_search_query = request.GET.get("customerSearch")
        print(customer_search_query)
        if customer_search_query:
            customers = Customer.search(customer_search_query)
        else:
            customers = Customer.objects.all()

        transfer_search_query = request.GET.get("transferSearch")
        print(transfer_search_query)
        if transfer_search_query:
            transfers = Transfer.search(transfer_search_query)
        else:
            transfers = Transfer.objects.all()

        context = {
            "bank_accounts": bank_accounts,
            "user_accounts": user_accounts,
            "customers": customers,
            "transfers": transfers,
        }
        return render(request, "user/dashboard-staff.html", context)
    else:
        # check if user is 2fa verified
        # if request.user.is_verified():

        accounts = Account.objects.filter(user=request.user, loanaccount__isnull=True)
        loans = request.user.customer.loans
        rank = request.user.customer.rank
        context = {
            "user": request.user.first_name,
            "accounts": accounts,
            "loans": loans,
            "rank": rank,
        }
        return render(request, "user/dashboard.html", context)
    # else:

    # enforce two-factor
    # TODO: keep commented while coding in progress
    # return redirect("/account/two_factor/setup/")


@login_required
def account_details(request, pk):
    if request.user.is_staff:
        account = get_object_or_404(Account, pk=pk)
    else:
        account = get_object_or_404(Account, user=request.user, pk=pk)

    context = {
        "account": account,
    }
    return render(request, "account/detail.html", context)


# we use this to have overview or manage profile
@login_required
def customer_detail(request, pk):
    if not request.user.is_staff:
        return

    customer = get_object_or_404(Customer, pk=pk)
    # we prepopulate user form
    if request.method == "GET":
        user_form = NewUserForm(instance=customer.user)
        # password cannot be updated
        user_form.fields["password"].widget = forms.HiddenInput()
        customer_form = CustomerForm(instance=customer)
    elif request.method == "POST":
        user_form = NewUserForm(request.POST, instance=customer.user)
        # password cannot be updated
        user_form.fields["password"].widget = forms.HiddenInput()
        customer_form = CustomerForm(request.POST, instance=customer)
        if user_form.is_valid() and customer_form.is_valid():
            user_form.save()
            customer_form.save()  # updates existing instance or creates new it theres no any

    context = {
        "customer": customer,
        "user_form": user_form,
        "customer_form": customer_form,
    }
    return render(request, "user/detail.html", context)


@login_required
def create_account(request, pk):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = AccountForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            account = Account.objects.create(
                user=User.objects.get(id=pk), name=form.cleaned_data["account_name"]
            )
            account.save()

        return redirect(f"/customer/{pk}")

    # if a GET (or any other method) we'll create a blank form
    else:
        context = {"form": AccountForm()}
        return render(request, "account/create.html", context)


@login_required
def disable_account(request, pk):
    # TODO fix this, condition should be before
    account = get_object_or_404(Account, pk=pk)
    account.is_disabled = True
    print(account.is_disabled)
    account.save()
    user = User.objects.get(username=account.user).id
    # TODO: disable account instead of deleting for loans
    if account.balance > 0:
        account.is_disabled = False
        context = {"error": "Cannot disable account with balance greater than 0.00"}
        return render(request, "error.html", context)
    return redirect(f"/customer/{user}")


@login_required
# disable customer
def disable_user(request, pk):
    customer = get_object_or_404(User, pk=pk)
    user = User.objects.get(username=customer)
    print(user.pk)
    account = Account.objects.filter(user_id=pk)
    # if customes has no accounts in the bank
    if not account:

        user.is_active = False
        print(user.is_active)
        user.customer.is_disabled = True
        user.save()
        user.customer.save()
        return redirect("/dashboard")
    else:
        # if any account is still active, we cannot disable user
        for account in account:
            print(account)
            if account.is_disabled == False:
                user.is_active = True
                print("not disabled")
                user.customer.is_disabled = False
                print("cannot disable")
                context = {"error": "Cannot delete user with active accounts"}
                return render(request, "error.html", context)

            else:
                user.is_active = False
                print("disabled")
                user.customer.is_disabled = True
                user.save()
                user.customer.save()
                print("disable")
                return redirect("/dashboard")
    # TODO: disabled users are greyed out in dashboard


@login_required
def make_loan(request):
    if request.method == "POST":
        request.user.customer.make_loan
        amount = Decimal(request.POST["amount"])
        name = request.POST["name"]
        request.user.customer.make_loan(amount, name)
        print(f"Loan amount is:{amount}")

        return redirect("/dashboard")
    return render(request, "transfer/loan.html")


@login_required
def create_transfer(request):
    if request.method == "POST":
        form = TransferForm(request.POST)
        form.fields["debit_account"].queryset = Account.objects.filter(
            user=request.user
        )
        if form.is_valid():
            amount = form.cleaned_data["amount"]
            debit_account = Account.objects.get(
                pk=form.cleaned_data["debit_account"].pk
            )
            debit_text = form.cleaned_data["debit_text"]
            credit_account = Account.objects.get(pk=form.cleaned_data["credit_account"])
            credit_text = form.cleaned_data["credit_text"]
            try:
                transfer = Transfer.transfer(
                    amount, debit_account, debit_text, credit_account, credit_text
                )
                print(f"Transfer:{transfer}")
                return redirect("/dashboard")

            except InsufficientFunds:
                context = {
                    "title": "Transfer Error",
                    "error": "Insufficient funds for transfer.",
                }
                return render(request, "error.html", context)
    else:
        form = TransferForm()
    form.fields["debit_account"].queryset = Account.objects.filter(user=request.user)
    context = {
        "form": form,
    }
    return render(request, "transfer/create.html", context)


@login_required
def pay_loan(request):
    if request.method == "POST":
        form = LoanPaymentForm(request.POST)
        form.fields["debit_account"].queryset = Account.objects.filter(
            user=request.user, loanaccount__isnull=True
        )
        form.fields["credit_account"].queryset = LoanAccount.objects.filter(
            user=request.user
        )
        if form.is_valid():
            amount = form.cleaned_data["amount"]
            debit_account = form.cleaned_data["debit_account"]
            credit_account = form.cleaned_data["credit_account"]
            try:
                transfer = Transfer.pay_loan(amount, debit_account, credit_account)
                print(f"Loan payment:{transfer}")
                return redirect("/dashboard")

            except InsufficientFunds:
                context = {
                    "title": "Transfer Error",
                    "error": "Insufficient funds for transfer.",
                }
                return render(request, "error.html", context)
    else:
        form = LoanPaymentForm()
    form.fields["debit_account"].queryset = Account.objects.filter(
        user=request.user, loanaccount__isnull=True
    )
    form.fields["credit_account"].queryset = LoanAccount.objects.filter(
        user=request.user
    )
    context = {
        "form": form,
    }
    return render(request, "transfer/loan-payment.html", context)


# https://docs.djangoproject.com/en/4.1/topics/auth/default/
def logout_view(request):
    print("logout")
    logout(request)
    # redirect to login 2fa
    return redirect("two_factor:login")


@login_required
def create_customer(request):
    print(request)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        new_user_form = NewUserForm(request.POST)
        customer_form = CustomerForm(request.POST)
        # check whether it's valid:
        if new_user_form.is_valid() and customer_form.is_valid():
            # process the data in form.cleaned_data as required
            username = new_user_form.cleaned_data["username"]
            first_name = new_user_form.cleaned_data["first_name"]
            last_name = new_user_form.cleaned_data["last_name"]
            email = new_user_form.cleaned_data["email"]
            password = new_user_form.cleaned_data["password"]
            rank = customer_form.cleaned_data["rank"]
            phone = customer_form.cleaned_data["phone"]

        try:
            # create new user in db
            new_user = User.objects.create_user(
                username=username,
                password=password,
                email=email,
                first_name=first_name,
                last_name=last_name,
            )
            new_user.save()

            new_customer = Customer.objects.create(
                user=new_user, rank=rank, phone=phone
            )
            new_customer.save()
            return redirect("/dashboard")

        except:
            # TODO: add error message
            pass

    # if a GET (or any other method for now) we'll create a blank form
    else:
        print("new form")
        new_user_form = NewUserForm()
        customer_form = CustomerForm()
    context = {
        "new_user_form": new_user_form,
        "customer_form": customer_form,
    }
    return render(request, "user/new_customer.html", context)


@login_required
def generate_pdf(request, pk):

    if request.user.is_staff:
        account = get_object_or_404(Account, pk=pk)
    else:
        account = get_object_or_404(Account, user=request.user, pk=pk)

    context = {"account": account}

    pdf = render_to_pdf("pdf/account.html", context)

    return HttpResponse(pdf, content_type="application/pdf")
