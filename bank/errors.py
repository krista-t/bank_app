class InsufficientFunds(Exception):
    pass


class MoreThanLoanAmount(Exception):
    pass
