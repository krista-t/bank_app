# Generated by Django 4.1.3 on 2022-12-11 15:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0027_alter_customer_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='mfa_token',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
