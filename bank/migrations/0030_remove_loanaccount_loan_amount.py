# Generated by Django 4.1.3 on 2023-01-14 18:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("bank", "0029_remove_customer_mfa_token"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="loanaccount",
            name="loan_amount",
        ),
    ]
