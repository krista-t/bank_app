from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from rest_framework.response import Response

from xhtml2pdf import pisa


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return result.getvalue()
    return None


# dictionary mapping bank id's to urls for api requests
bank_urls = {
    1: "http://127.0.0.1:8000/api/v1/transaction",
    2: "http://127.0.0.1:3333/api/v1/transaction",
}


# dictionary mapping responses to response objects
responses = {
    "no_auth": Response(
        "User Not authenticated",
        status=401,
    ),
    "account_not_found": Response(
        {"error": "Sender account does not exist"},
        status=400,
    ),
    "receiver_account_not_found": Response(
        {"error": "Receiver account does not exist or token already exists"},
        status=400,
    ),
    "no_idempotency_token": Response(
        {"error": "No idempotency token provided"},
        status=400,
    ),
    "idem_token_exists": Response(
        {"error": "Transfer with this token already exists"},
        status=400,
    ),
    "request_failed": Response(
        {"error": "Request failed on the receiving bank"},
        status=500,
    ),
    "transfer_confirmation_failed": Response(
        {"error": "Transfer confirmation failed"},
        status=500,
    ),
    "transfer_failed": Response({"error": "Transfer failed"}, status=500),
    "insufficient_funds": Response({"error": "Insufficient funds"}, status=400),
}
