from rest_framework import generics, views
from .models import ExternalTransfer, Account, Transfer
from .serializer import (
    ExternalTransferSerializer,
    UpdateExternalTransferStatusSerializer,
    AccountSerializer,
)
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from django.core import serializers
from django.shortcuts import redirect
from .utils import responses
import requests as req
import uuid


class Transaction(generics.CreateAPIView):
    queryset = ExternalTransfer.objects.all()
    serializer_class = ExternalTransferSerializer

    def post(self, request, *args, **kwargs):
        """if not request.user.is_authenticated:
        return responses.no_auth"""

        if not request.data["sender_account"]:
            return responses["account_not_found"]

        sender_account = find_account_in_local_db(request.data["sender_account"])
        if not sender_account:
            return responses["account_not_found"]

        if not request.data["token"]:
            return responses["no_idempotency_token"]

        if sender_account.balance < request.data["amount"]:
            return responses["insufficient_funds"]

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        idem_token = find_transfer_record_in_local_db(request.data["token"])

        if idem_token:
            print(
                f"🔵 Token {idem_token} exists in database, returning existing transfer"
            )
            return Response(serializer.data, status=200)

        print(f"Token {request.data['token']} does not exist in database!")

        url_bank_1 = "http://127.0.0.1:8000/api/v1/external-transfer"
        url_bank_2 = "http://127.0.0.1:3333/api/v1/external-transfer"
        remote_account_res = req.post(url_bank_2, data=request.data)

        # chekc that receiver account exists in remote bank and that a transfer with the same token does not exist
        if remote_account_res.status_code != 200:
            print("❌ Account does not exist in remote bank or token already exists")
            return responses["receiver_account_not_found"]

        remote_account = remote_account_res.json()
        print(f"✅ Remote account {remote_account} found")

        try:
            print("✅ Creating external outbound transfer in local database")
            re = serializer.create(serializer.validated_data)
            print(re)
        except:
            # TODO: set status of remote transfer to failed
            return responses["request_failed"]

        print("✅ Confirming remote transfer")
        confirm_payment_res = req.put(
            url_bank_2 + "/" + request.data["token"] + "?type=receiving"
        )

        if confirm_payment_res.status_code != 200:
            print("❌ Error while confirming remote transfer")
            return responses["transfer_confirmation_failed"]

        print("✅ Confirming local transfer")
        confirm_payment_res_local = req.put(
            url_bank_1 + "/" + request.data["token"] + "?type=sending"
        )

        if confirm_payment_res_local.status_code != 200:
            print("❌ Error while confirming local transfer")
            return responses["transfer_confirmation_failed"]

        return Response(serializer.data, status=201)


class ExternalTransferRecordCreate(generics.CreateAPIView):
    def post(self, request):
        account_in_local_db = find_account_in_local_db(request.data["receiver_account"])
        if not account_in_local_db:
            return responses["receiver_account_not_found"]

        idem_token = find_transfer_record_in_local_db(request.data["token"])

        if idem_token:
            print(f"❌ Token {idem_token} already exists in database!")

            # TODO: set local transfer status to failed
            return responses["idem_token_exists"]

        print(f"✅ Creating inbound external transfer {request.data['token']}")

        try:
            serializer = ExternalTransferSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            re = serializer.create(serializer.validated_data)
            print(re)

            serializer = AccountSerializer(account_in_local_db)
            json = JSONRenderer().render(serializer.data)
        except Exception as e:
            print(e)
            return responses["request_failed"]

        return Response(json, status=200)


class ExternalTransferRecordUpadte(generics.UpdateAPIView):
    # confirming transfer
    def update(self, request, token):
        # TODO: authorize request
        print("✅ Confirming transfer")
        try:
            local_transfer = find_transfer_record_in_local_db(token)

            local_transfer.status = ExternalTransfer.Status.CONFIRMED

            local_transfer.save()
        except Exception as e:
            print(e)
            return responses["transfer_confirmation_failed"]

        try:
            type = request.query_params.get("type")

            print(type)

            transfer_id = uuid.uuid4()
            Transfer(
                transfer_id=transfer_id,
                amount=type == "receiving"
                and local_transfer.amount
                or -local_transfer.amount,
                account_id=type == "receiving"
                and local_transfer.sender_account
                or local_transfer.receiver_account,
                text=local_transfer.text,
            ).save()
        except Exception as e:
            print(e)
            # TODO: set local transfer and remote status to failed
            return responses["transfer_failed"]

        return Response(status=200)


def find_account_in_local_db(pk):
    try:
        account = Account.objects.get(id=pk)
        return account
    except Account.DoesNotExist:
        return False


def find_transfer_record_in_local_db(token):
    try:
        return ExternalTransfer.objects.get(token=token)
    except ExternalTransfer.DoesNotExist:
        return False
