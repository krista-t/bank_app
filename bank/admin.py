from django.contrib import admin
from .models import Customer, Account, Transfer, LoanAccount, ExternalTransfer

admin.site.register(Customer)
admin.site.register(Account)
admin.site.register(Transfer)
admin.site.register(LoanAccount)
admin.site.register(ExternalTransfer)


# Register your models here.
