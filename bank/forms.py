from django import forms
from django.forms import ModelForm  # new
from django.contrib.auth.models import User  # new
from bank.models import Customer, Account
from django.core.exceptions import ObjectDoesNotExist


# https://docs.djangoproject.com/en/4.1/topics/forms/modelforms/
class NewUserForm(forms.ModelForm):
    # use meta to include fields you choose from associated model
    class Meta:
        model = User
        fields = ["username", "email", "password", "first_name", "last_name"]
        # use the built-in password widget for password input
        widgets = {
            "password": forms.PasswordInput(),
        }
        # remove buildin text for username
        help_texts = {
            "username": None,
        }

        # check if username is unique
        def clean(self):
            super().clean()
            username = self.cleaned_data.get("username")
            if User.objects.filter(username=username):
                self._errors["username"] = self.error_class(
                    ["Username already exists."]
                )
            return self.cleaned_data


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ["phone", "rank"]
        widgets = {
            "rank": forms.Select(attrs={"class": "form-control"}),
        }


class AccountForm(forms.Form):
    account_name = forms.CharField(label="Account name", max_length=100)


class TransferForm(forms.Form):
    amount = forms.DecimalField(label="Amount", max_digits=10, min_value=0)
    debit_account = forms.ModelChoiceField(
        label="Debit Account", queryset=Customer.objects.none() or None
    )
    debit_text = forms.CharField(label="Debit Account Text", max_length=25)
    credit_account = forms.IntegerField(
        label="Credit Account Number",
    )
    credit_text = forms.CharField(label="Credit Account Text", max_length=25)

    def clean(self):
        super().clean()

        # Ensure credit account exist
        credit_account = self.cleaned_data.get("credit_account")
        try:
            Account.objects.get(pk=credit_account)
        except ObjectDoesNotExist:
            self._errors["credit_account"] = self.error_class(
                ["Credit account does not exist."]
            )

        return self.cleaned_data


class LoanPaymentForm(forms.Form):
    amount = forms.DecimalField(label="Amount", max_digits=10, min_value=0)
    debit_account = forms.ModelChoiceField(
        label="Debit Account", queryset=Customer.objects.none() or None
    )
    credit_account = forms.ModelChoiceField(
        label="Loan Account", queryset=Customer.objects.none() or None
    )

    def clean(self):
        super().clean()

        # Ensure credit account exist
        credit_account = self.cleaned_data.get("credit_account")
        try:
            Account.objects.get(pk=credit_account.pk)
        except ObjectDoesNotExist:
            self._errors["credit_account"] = self.error_class(
                ["Credit account does not exist."]
            )

        return self.cleaned_data
