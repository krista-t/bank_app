from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views
from .api import Transaction, ExternalTransferRecordCreate, ExternalTransferRecordUpadte

# from .views import MyViewSet

# https://docs.djangoproject.com/en/4.1/topics/auth/default/#module-django.contrib.auth.views
urlpatterns = [
    path("accounts/", include("django.contrib.auth.urls")),
    path("", views.home),
    path("dashboard/", views.dashboard, name="dashboard"),
    path("dashboard-staff/", views.dashboard, name="dashboard-staff"),
    path("account/create/<int:pk>", views.create_account, name="create-account"),
    path("account/disable/<int:pk>", views.disable_account, name="disable_account"),
    path("account/<int:pk>/", views.account_details, name="account"),
    path("account/generate-pdf/<int:pk>", views.generate_pdf, name="generate-pdf"),
    path("customer/<int:pk>/", views.customer_detail, name="customer"),
    path("customer/disable/<int:pk>", views.disable_user, name="disable_user"),
    path("customer/create/", views.create_customer, name="create_customer"),
    path("transfer/create", views.create_transfer, name="create-transfer"),
    path("transfer/loan-payment", views.pay_loan, name="loan-payment"),
    path("transfer/loan", views.make_loan, name="make_loan"),
    path(
        "password_reset/",
        auth_views.PasswordResetView.as_view(
            template_name="registration/password_reset.html"
        ),
        name="password_reset",
    ),
    path(
        "password_reset_sent/",
        auth_views.PasswordResetDoneView.as_view(
            template_name="registration/password_reset_sent.html"
        ),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="registration/password_form.html"
        ),
        name="password_reset_confirm",
    ),
    path(
        "password_reset_complete/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="registration/passreset_complete.html"
        ),
        name="password_reset_complete",
    ),
    path("api/v1/transaction", Transaction.as_view()),  # post
    path(
        "api/v1/external-transfer",
        ExternalTransferRecordCreate.as_view(),
    ),
    path(
        "api/v1/external-transfer/<uuid:token>",
        ExternalTransferRecordUpadte.as_view(),
    ),
]
