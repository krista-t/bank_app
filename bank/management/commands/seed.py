# https://docs.djangoproject.com/en/4.1/howto/custom-management-commands/

from os import access
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from bank.models import Account, Customer, Transfer
from django.utils import timezone


class Command(BaseCommand):
    def handle(self, *args, **options):
        print("Seeding the database...")

        dummy_user_1 = User.objects.create_user(
            "user", email="user@user.com", password="user"
        )
        dummy_user_1.first_name = "John"
        dummy_user_1.last_name = "Doe"
        dummy_user_1.save()
        dummy_customer_1 = Customer(user=dummy_user_1, phone="124616", rank = "Gold")
        dummy_customer_1.save()
        dummy_account_1 = Account.objects.create(user=dummy_user_1, name="Main account")
        dummy_account_2 = Account.objects.create(
            user=dummy_user_1, name="Secondary account"
        )
        dummy_account_1.save()
        dummy_account_2.save()

        dummy_user = User.objects.create_user(
            "dummy", email="dummy@dummy.com", password="mirror12"
        )
        dummy_user.first_name = "Dummy"
        dummy_user.last_name = "Dimwit"
        dummy_user.save()
        dummy_customer = Customer(user=dummy_user, phone="556656", rank = "Silver")
        dummy_customer.save()
        dummy_account = Account.objects.create(user=dummy_user, name="Checking account")
        dummy_account.save()

        bank_user = User.objects.create_user(
            "bank", email="bank@bank.com", password="bank"
        )
        bank_user.is_active = True
        bank_user.is_staff = True
        bank_user.save()
        ipo_account = Account.objects.create(user=bank_user, name="Bank IPO Account")
        ops_account = Account.objects.create(user=bank_user, name="Bank OPS Account")
        Transfer.transfer(
            10_000_000,
            ipo_account,
            "Operational Credit",
            ops_account,
            "Operational Credit",
            is_loan=True,
        )

        Transfer.transfer(
            5_000,
            ops_account,
            "Operational Credit",
            dummy_account_1,
            "Initial balance",
            is_loan=True,
        )
        Transfer.transfer(
            0,
            ops_account,
            "Operational Credit",
            dummy_account_2,
            "Initial balance",
            is_loan=True,
        )
        Transfer.transfer(
            3_000,
            ops_account,
            "Operational Credit",
            dummy_account,
            "Initial balance",
            is_loan=True,
        )
