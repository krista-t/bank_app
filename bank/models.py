from __future__ import annotations
from decimal import Decimal
from django.conf import settings
from django.db import models, transaction
from django.db.models import Q
from django.db.models.query import QuerySet
from django.contrib.auth.models import User
from .errors import InsufficientFunds, MoreThanLoanAmount
import requests as req


import uuid


# Create your models here.
class Customer(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.PROTECT)
    phone = models.CharField(max_length=35, db_index=True)
    is_disabled = models.BooleanField(default=False)

    # https://docs.djangoproject.com/en/4.1/ref/models/fields/
    class CustomerRank(models.TextChoices):
        BASIC = "basic"
        SILVER = "silver"
        GOLD = "gold"

    rank = models.CharField(
        max_length=20, choices=CustomerRank.choices, default=CustomerRank.BASIC
    )

    @property
    def full_name(self) -> str:
        return f"{self.user.first_name} {self.user.last_name}"

    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user, loanaccount__isnull=True)

    @property
    def loans(self) -> QuerySet:
        return LoanAccount.objects.filter(user=self.user)

    @property
    def can_make_loan(self):
        return self.rank in [self.CustomerRank.SILVER, self.CustomerRank.GOLD]

    @classmethod
    def search(cls, search_query):
        return cls.objects.filter(
            Q(user__username__icontains=search_query)
            | Q(user__last_name__icontains=search_query)
            | Q(user__first_name__icontains=search_query)
            | Q(user__email__icontains=search_query)
        )[:15]

    def make_loan(self, amount, name) -> QuerySet:
        assert amount >= 0, "Negative amount not allowed for loan."
        assert self.can_make_loan, "User rank does not allow for making loans."

        loan_account = LoanAccount.objects.create(user=self.user, name=name)

        # account to be credited from the loan
        default_account = Account.objects.filter(user=self.user).first()
        assert True, "User rank does not allow for making loans."
        assert amount >= 0, "Negative amount not allowed for loan."
        Transfer.transfer(
            amount,
            loan_account,
            f"Loan paid out to account {default_account}",
            default_account,
            f"Credit from loan {loan_account.pk}: {loan_account.name}",
            is_loan=True,
        )

    def __str__(self):
        return f"{self.user_id}: {self.full_name}"


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=50, db_index=True, null=True)
    is_disabled = models.BooleanField(default=False)

    class Meta:
        get_latest_by = "pk"

    @property
    def transfers(self) -> QuerySet:
        return Transfer.objects.filter(account_id=self.pk)

    @property
    def balance(self) -> Decimal:
        return self.transfers.aggregate(models.Sum("amount"))["amount__sum"] or Decimal(
            0
        )

    def __str__(self):
        return f"PK: {self.pk}, User: {self.user}, Name: {self.name}"


class LoanAccount(Account):
    @property
    def loan_amount(self) -> Decimal:
        first_tranfer = self.transfers.first()

        # print(first_tranfer)

        return abs(first_tranfer.amount) or 0

    @property
    def amount_paid(self):
        return (
            self.transfers.filter(amount__gt=0).aggregate(models.Sum("amount"))[
                "amount__sum"
            ]
            or 0
        )

    @property
    def percentage_paid(self):
        return format((self.amount_paid / self.loan_amount) * 100, ".2f")

    @property
    def remaining(self):
        return self.loan_amount - self.amount_paid

    @property
    def is_paid(self):
        return self.amount_paid >= self.loan_amount


class Transfer(models.Model):
    transfer_id = models.UUIDField(default=uuid.uuid4)
    account_id = models.IntegerField(null=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    current_amount = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, default=0
    )
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    text = models.TextField()

    @classmethod
    def transfer(
        cls,
        amount,
        debit_account,
        debit_text,
        credit_account,
        credit_text,
        is_loan=False,
    ) -> int:
        assert amount >= 0, "Negative amount not allowed for transfer."
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                print("DEBIT BALANCE", debit_account.balance)
                transfer_id = uuid.uuid4()
                current_amount = debit_account.balance - amount
                cls(
                    transfer_id=transfer_id,
                    amount=-amount,
                    current_amount=current_amount,
                    account_id=debit_account.pk,
                    text=debit_text,
                ).save()

                current_amount = credit_account.balance + amount
                print("CREDIT BALANCE", credit_account.balance)
                cls(
                    transfer_id=transfer_id,
                    amount=amount,
                    current_amount=current_amount,
                    account_id=credit_account.pk,
                    text=credit_text,
                ).save()
            else:
                raise InsufficientFunds
        return id

    @classmethod
    def pay_loan(cls, amount, debit_account, credit_account):
        if credit_account.balance + amount <= credit_account.loan_amount:
            return cls.transfer(
                amount=amount,
                debit_account=debit_account,
                debit_text=f"Payment on loan {credit_account.name}",
                credit_account=credit_account,
                credit_text="Loan payment",
            )
        else:
            raise MoreThanLoanAmount

    @classmethod
    def search(cls, search_query):
        return cls.objects.filter(
            Q(text__icontains=search_query)
            | Q(transfer_id__icontains=search_query)
            | Q(amount__icontains=search_query)
        )[:15]

    def __str__(self):
        return f"{self.amount} :: {self.transfer_id} :: {self.timestamp} :: {self.account_id} :: {self.text}"


class ExternalTransfer(models.Model):
    token = models.UUIDField(default=uuid.uuid4)
    sender_account = models.IntegerField(null=True)
    receiver_account = models.IntegerField(null=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    text = models.TextField()

    class Status(models.TextChoices):
        PENDING = "pending"
        CONFIRMED = "confirmed"
        REJECTED = "rejected"

    status = models.CharField(
        max_length=10, choices=Status.choices, default=Status.PENDING
    )

    @classmethod
    def handle_transaction(cls, token):

        """
        called in api.py,
        receives token from api.py and
        creates a transfer on local ledger table to register external transfers

        """
        external_transfer = ExternalTransfer.objects.get(token=token)
        amount = external_transfer.amount
        debit_account = external_transfer.sender_account
        debit_text = external_transfer.text
        credit_account = external_transfer.receiver_account
        external_transfer.receiver_account
        credit_text = f"External transfer to {external_transfer.receiver_account}"
        # TODO: external transfer
        Transfer.transfer(
            amount=amount,
            debit_account=debit_account,
            debit_text=debit_text,
            credit_account=credit_account,
            credit_text=credit_text,
            is_loan=False,
        )
        print(f"Transfer completed with token: {token}")
        return token

    def __str__(self):
        return f"Transfer: {self.amount} :: {self.status} - {self.token} From: {self.sender_account} To:{self.receiver_account}"
