# bank_app

This is the exam project for Backend with Python and Django class at KEA.

# starting a project

- make virtual enviroment: python -m venv /pathtoyourvenv
- activate virtual enviroment: source /pathtoyourvene/bin/activate
- install dependencies: pip install -r requirements.txt
- migrate data: python manage.py makemigrations, python manage.py migrate)
- add demodata to app: python manage.py seed
- start app in browser: python manage.py runserver

# communication over network

- copy project directory on local machine and follow steps for starting a project
- to start the copy app in browser run: python manage.py runserver 3333
- make sure that the sender and receiver accounts exist in both banks
- make sure to comment lines: 67, 68, 84, 85
- replace on line 60:

  """
  if not request.data['token']:
  with
  if not ExternalTransfer.objects.filter(token=request.data['token']):
  ""

# external transaction
- external transaction is made through https://www.django-rest-framework.org/ interface
- path to make post for external transaction: localhost/api/v1/transaction
- path to confirm external transaction: localhost/api/v1/status/RESTAPI_GENERATED_TOKEN

# CSS

https://watercss.kognise.dev/
